export class SVGBoard {
    private canvas: HTMLElement; // board
    private ns: string
    private x: number;
    private piece_size: number;
    private spacing: number;
    private colors = ["white","#dd281b","#ffc947"] 
    board: number[][]

    constructor() {

        let layout = {} as any

        console.log("Constructing..")
        this.x = 0
        this.ns = "http://www.w3.org/2000/svg";
        this.canvas = document.querySelector('.svg-canvas');

        let w = parseInt(getComputedStyle(this.canvas).getPropertyValue('width'))
        let h = parseInt(getComputedStyle(this.canvas).getPropertyValue('height'))
        layout = this.calculateLayouts(w, h)
        let xpad = layout.xpad
        let ypad = layout.ypad
        let radius = layout.radius

        console.log(`attributes are, bg ${this.canvas['style'].width}, height ${this.canvas.style.height}`)
        let rect = document.getElementById('gameboard-rect') as HTMLElement
        if (rect == null) { 
            rect = document.createElementNS(this.ns, "rect") as HTMLElement;
            this.setAttributes(rect, {'id':'gameboard-rect', 'x':'0', 'y':'0', 'width':w, 'rx':'5', 'ry':"5", 'height':h, 'fill':'#3E68DF', 'class':'svg-rect', 'z-index':15})
            console.log(`rect =${rect}`)
            this.canvas.appendChild(rect)
            for (let x=0; x < 7; x++)  {
                for (let y=0; y< 6; y++) {
                    let circ = document.createElementNS(this.ns, "circle");
                    this.setAttributes(circ, {'id':"circ"+x*6+y, 'cx': 2 * xpad + radius + (2*xpad + 2*radius)*x, 'cy': 2 * ypad + radius + (2 * ypad + 2 * radius)*y, 'r':radius,'fill':'#FFFFFF'})
                    this.canvas.appendChild(circ)
                }
            }      
        }  
        this.board = this.getTestBoard()
    }
    setAttributes(obj: any, attrs: any) {
        for(var a in attrs) {
            obj.setAttribute(a, attrs[a])
        }
    }
    calculateLayouts( width : number, height : number ) {
        // width is made up 51 units. 7 for each cirle (2 units = radius, 1 unit padding on each side of circle.)
        //      plus 2 units padding on left and right
        let xpad = width / 58 
        let ypad = height / 50
        let radius = xpad * 3  // we could make this an ellipse and count on the stylesheets to get the righ aspect ratio, but this is simpler.
        return {'xpad':xpad, 'ypad':ypad, 'radius':radius}
    }
    start() {
        console.log("Starting..")
        //this.life.drawGridOnCanvasContext(this.context);
        this.redraw()
    }
    setBoard(newBoard: number[][]) {
        this.board = newBoard
    }
    setWinner(player : number) {
        let p = document.getElementById('result')
        p.innerHTML = `winner = ${this.colors[player]}!`
    }
    getTestBoard() : number[][] {
        let m = {
            id: "Test1",
            playe11: "name1",
            player2: "name2",
            me : "player1",
            turn: "player1",
            moves : [ 
                0,1,0,1,2,2,2,3], 
            board: [ 
                [1,1,1,1,1,1], 
                [1,0,0,0,0,1],
                [1,1,0,0,1,1],
                [0,0,0,0,0,0],
                [0,0,0,2,2,2],
                [0,0,0,2,0,0],
                [2,2,2,2,2,2],
            ], 
            winner: "-"
        }
        
        return m.board 
    }
    /**
     * looks at the history of the entire game, and animates it with one move per 
     * millisecond interval
     * */ 
    private animateGame(jsonMessage : string, millisecond : number) {
        
    }
    private displayBoard(jsonMessage : string) {
        
    }
    
    redraw() {
        for (let  x = 0; x < 7; x++) {
            for (let y = 0; y < 6; y++) {
                let element = document.getElementById("circ"+x*6+y)
                element.setAttribute("fill", this.colors[this.board[x][5-y]]);
            }
        
       } 
    }

}
