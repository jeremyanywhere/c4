import {Board} from "./Board"
export class PlayerProxy {
    game_ID : string
    player_id : string 
    vs : string 
    url : string
    port : number
    descr : string
    response : string
    pinged : Boolean
    initalized : Boolean
    finished : Boolean

    constructor(url: string, port: number) {
        let s1 = ["silky", "big", "tiny", "evil", "shiny", "bad", "goodly", "annoying", "scary", "warty"];
        let s2 = ["glowing", "running", "fierce", "friendly", "jumping", "creeping", "flying", "hopping", "leaping", "stealthy", "Mc"];
        let s3 = ["cat", "hedgehog", "dwarf", "goblin", "wizard", "mermaid", "wolf", "tiger", "lion", "serpent", "hero", "bat", "hog"];
        this.player_id  = s1[Math.floor(Math.random() * s1.length)]
            + "-" + s2[Math.floor(Math.random() * s2.length)]
            + "-" + s3[Math.floor(Math.random() * s3.length)];
        let move = Math.floor(Math.random() * 7);
        this.url = url
        this.port = port
        // construct end point. Load config file or something. 
    }
    ping() {

    }
    init(game_ID : string, which : number) {
        let j = {cock:"melud"}
        //let res = this.httpRequestResponse(this.endpoint,this.port,"ping",j)

        // if ping isn't good, error here.. 
        //this.game_ID = game_ID
        //let k = {game_id : game_ID, which_player:which, message: "Init.."}
        //res = this.httpRequestResponse(this.endpoint,this.port,"init",k)
        
        //console.log(`response from init message is: ${res.player_id}`)
        //if (this.player_id != res.player_id) {
        //    throw Error("Inconsistency in Ping / Init") 
        //}   
        //this.descr = res.descr
    }

    getMove(board : Board, me: number, turn : number, vs : string, moves : number[], winner : number ) : number {
        // construct message. 
        let players = []
        players[me-1] = this.player_id; // me-1 converts the player no. to an array index. 
        players[Math.abs((me-2)%2)] = vs 
        let message = {
            "id": this.game_ID,
            "players":players,
            "me" : this.player_id,
            "turn": turn,
            "moves" : moves,
            "board" : board,
            "error" : "",
            "winner" : winner
        }
        // send message.. 
        return 0;
    }
    endGame(result : string) {

    }

}
