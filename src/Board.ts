
export class Board {
    
    COLS : number
    ROWS : number
    board : number[][]
    constructor() {
        this.COLS = 7;
        this.ROWS = 6;
        this.board = []
        for (let col = 0; col < this.COLS; col++) {
            this.board[col] = []
            for (let row = 0; row < this.ROWS; row++) {
                this.board[col][row] = 0;
            }
        }
    }
    dump() {
        console.log("dumping->")
        for (let row = 0; row < this.ROWS; row++) {
            let line = "";
            for (let col = 0; col < this.COLS; col++) {
                line += this.board[col][this.ROWS - (row + 1)];
            }
            console.log(line);
        }
        console.log("-------");
    }
    isMoveLegal(col:number) {
        console.log(`col is ${col}`)
        return this.board[col][5] == 0
    }
    move(col:number, player:number) {
        for (let row = 0; row < this.ROWS; row++) {
            if (this.board[col][row] == 0) {
                this.board[col][row] = player;
                return row;
            }
        }
        return -1;
    }
    checkForWin() {
        for (let row = 0; row < this.ROWS; row++) {
            let streak = 0;
            let player = -1;
            for (let col = 0; col < this.COLS; col++) {
                if (this.board[col][row] == 0) {
                    streak = 0;
                    player = -1;
                }
                else {
                    if (this.board[col][row] == player) {
                        streak++;
                        if (streak > 3) {
                            return player;
                        }
                    }
                    else {
                        player = this.board[col][row];
                        streak = 1;
                    }
                }
            }
        }
        for (let col = 0; col < this.COLS; col++) {
            let streak = 0;
            let player = -1;
            for (let row = 0; row < this.ROWS; row++) {
                if (this.board[col][row] == 0) {
                    streak = 0;
                    player = -1;
                }
                else {
                    if (this.board[col][row] == player) {
                        streak++;
                        if (streak > 3) {
                            return player;
                        }
                    }
                    else {
                        player = this.board[col][row];
                        streak = 1;
                    }
                }
            }
        }
        for (let col = 0; col < this.COLS; col++) {
            let streak = 0;
            let player = -1;
            for (let row = 0; row < this.ROWS; row++) {
                player = this.board[col][row];
                if (player != 0) {
                    streak = 1;
                    let x = 0;
                    let dcne = col + 1; // diagonal North East
                    let drne = row + 1;
                    while (dcne + x < this.COLS && drne + x < this.ROWS) {
                        if (this.board[dcne + x][drne + x] == 0) {
                            break;
                        }
                        if (this.board[dcne + x][drne + x] == player) {
                            streak++;
                            if (streak > 3) {
                                return player;
                            }
                        }
                        else {
                            player = this.board[dcne + x][drne + x];
                            streak = 1;
                        }
                        x++;
                    }
                    player = this.board[col][row];
                    streak = 1;
                    x = 0;
                    let dcnw = col - 1;
                    let drnw = row + 1;
                    while (dcnw - x >= 0 && drnw + x < this.ROWS) {
                        if (this.board[dcnw - x][drnw + x] == 0) {
                            break;
                        }
                        if (this.board[dcnw - x][drnw + x] == player) {
                            streak++;
                            if (streak > 3) {
                                return player;
                            }
                        }
                        else {
                            player = this.board[dcnw - x][drnw + x];
                            streak = 1;
                        }
                        x++;
                    }
                }
            }
        }
        return 0;
    }
}
function testBoard() {
    let tb = new Board()
    let col = [4,1,2,3,4,1,2,3,4,1,2,3,4,1,2,3]
    let player = [1,2,1,2,1,1,1,1,1,2,1,1,2,2,2,2]
    tb.dump()
    for (let x = 0; x < col.length; x++) {
        tb.move(col[x],player[x])
    }
    tb.dump()
    console.log(`check for win - ${tb.checkForWin()}`)
}
