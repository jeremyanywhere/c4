
import {SVGBoard} from "./SVGBoard"
import {Board} from "./Board"
import {PlayerProxy} from "./PlayerProxy"
export class Game {
    canvas : SVGBoard
    board : Board
    game_ID : string
    moves : number []
    players  : PlayerProxy[]
    current : number
    winner : number
    turns : number
    gameover : false
    
    constructor(p1 : PlayerProxy, p2 : PlayerProxy) {
        this.canvas = new SVGBoard()
        this.game_ID = Date() 
        this.board = new Board()
        this.gameover = false
        this.turns = 0 
        this.current = 1
        this.winner = 0
        this.players = []
        this.players[0] = null
        this.players[1] = p1
        this.players[2] = p2
        this.canvas.start()
    }
    // a kind of state machine.
    // messages here:
    pingRequest(player: number) {
        let message = {}
        this.sendMessage(player, "ping", message, "pingResponse")
    }
    pingResponse(player: number, result: any) {
        this.players[player].pinged = true
        this.gamePlay()
    }
    initRequest(player: number) {
        let msg =  {game_id : this.game_ID, which_player : player, message : "some message about the game.. "}
        this.sendMessage(player, "init", msg, "initResponse")
    }
    initResponse(player: number, result: any) {
        this.players[player].initalized = true
        console.log(`** Player ${player} initialized. Message - ${result.player_id}`)
        this.gamePlay()
    }
    moveRequest(player : number) {
        let msg = {
            id: this.game_ID,
            me: this.players[player].player_id,
            opponent: this.players[player%2+1].player_id,
            player_no: player,
            turns: this.turns,
            moves : this.moves,
            board : this.board.board,
            error : "",
            winner : 0
        }
        this.sendMessage(player, "move", msg, "moveResponse")
    }
    moveResponse(player : number, result: any ) {
        let move = result.move
        console.log(`And the move is ${move} `)
        if (!this.board.isMoveLegal(move)) {
            // send error message to last player.
            // send end game message to all players with note.. 
        }
        // this is where we do the players moves, check for winner etc. 
        this.board.move(move, player)
        this.canvas.setBoard(this.board.board)
        this.canvas.redraw()
        this.winner = this.board.checkForWin()
        this.turns += 1
        this.nextPlayer()
        setTimeout(this.gamePlay.bind(this), 1000)
    }
    gameoverRequest(player: number) {
        console.log(`player is ${player}`)
        let msg = {   winner : this.winner,
            turns : this.turns,
            board : this.board.board
        }
        this.sendMessage(player, "gameover", msg, "gameoverResponse")
    }
    gameoverResponse(player : number, result: any ) {
        console.log(`acknowledgement ${result.player_id} `)
        this.players[player].finished = true
        this.gamePlay()
    }
    gamePlay() {
        if(!this.players[1].pinged) {
            this.pingRequest(1)
        } else if (!this.players[2].pinged) {
            this.pingRequest(2)
        } else if (!this.players[1].initalized) {
            this.initRequest(1)
        } else if (!this.players[2].initalized) {
            this.initRequest(2)
        } else if (this.board.checkForWin()==0) {
            this.moveRequest(this.current)
        } else if (!this.players[1].finished) {
            this.gameoverRequest(1)
        } else if (!this.players[2].finished) {
            this.gameoverRequest(2)
        } else {
            console.log("Looks like the game is over.")
            console.log(`Winner seems to be ${this.winner}`)
            this.board.dump()
            this.canvas.setWinner(this.winner)
        }
    }
    nextPlayer() {
        this.current =(this.current%2)+1
    }
    end(g: Game) {
        console.log("Ya. We are done. ")
    }

    private sendMessage(player: number, command: string, message : any, next : string) {
        console.log(`player is ${this.players[player]}`)
        let jm = JSON.stringify(message)
        let myRequest =  new XMLHttpRequest();
        let prt = this.players[player].port
        let playerURL = ""
        if (prt) {
            playerURL = this.players[player].url + ":" + prt + "/" + command
        } else {
            playerURL = this.players[player].url + "/" + command
        }
        console.log(`URL requested is: ${playerURL}`)
        var that = this as any
        myRequest.open('POST', playerURL);
        myRequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        myRequest.setRequestHeader("Airplane", "application/json;charset=UTF-8");
        myRequest.onreadystatechange = function () { 
            if (myRequest.readyState === 4) {
                let res = JSON.parse(myRequest.responseText)   
                that[next](player, res)
            }   
        }
        myRequest.send(jm)
    }
}

