const webpack = require('webpack');
const path = require('path');
const autoprefixer = require('autoprefixer');

module.exports = [
  {
    entry: './src/styles/appstylez.scss',
    output: {
      // This is necessary for webpack to compile
      // But we never use style-bundle.js
      filename: 'style-bundle.js',
    },
    module: {
      rules: [
        {
          test: /\.s(a|c)ss$/,
          use:[
            {
              loader: 'file-loader',
              options: {
                name: 'bundle.css',
              },
            },
            {loader: 'extract-loader'},
            {loader: 'css-loader'}, 
            {loader: 'sass-loader',
              options: {
              includePaths: ['./node_modules'],
              implementation: require('dart-sass'),
              fiber: require('fibers')}
            }]
        }
      ]
    }
  },
  {
    entry: './src/Connect4.ts',
    output: {
      filename: 'bundle.js',
      path: __dirname + '/dist',
      library: 'c4'
    },
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          use: 'ts-loader',
          exclude: /node_modules/
        }]
    } ,
    resolve: {
      extensions: [ '.tsx', '.ts', '.js' ]
    }
  }
]